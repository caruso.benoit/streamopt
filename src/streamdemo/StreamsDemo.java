package streamdemo;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import data.Enfant;
import data.Femme;
import data.President;

public class StreamsDemo {

	
	
	public static void main(String[] args) {
		List<Femme> wivesOfTonton = Arrays.asList(
					new Femme("Danielle", Arrays.asList(
							new Enfant("Jean-Christophe", 72),
							new Enfant("Gilbert", 69)
							)),
					new Femme("Anne", Arrays.asList(
							new Enfant("Mazarine", 44)
							))
				);
		
		List<President> presidents = Arrays.asList(
				new President("Charly", "De Gaulle", 1959, "UNR"),
				new President("Charly", "De Gaulle", 1966, "UDR"),
				new President("Georges", "Pompidou", 1969, "UDR"),
				new President("Valery", "Giscard d'estaing", 1974, "UDF"),
				new President("François", "Mitterand", 1981, "PS", wivesOfTonton),
				new President("François", "Mitterand", 1988, "PS", wivesOfTonton),
				new President("Jacques", "Chirac", 1995, "RPR"),
				new President("Jacques", "Chirac", 2002, "UMP"),
				new President("Nicolas", "Sarkozy", 2007, "UMP"),
				new President("François", "Hollande", 2012, "PS"),
				new President("Manu", "Macron", 2017, "LREM")
				);
		
		StreamsDemo.demo12(presidents.stream());
	}
	
	private static void demo1(Stream<President> presidents) {
		presidents.filter(p -> p.getElectionYear() < 2003)
				  .forEach(System.out::println);
	}
	private static void demo2(Stream<President> presidents) {
		presidents.filter(p -> p.getElectionYear() < 2003)
				  .map(p -> p.getFirstname())
				  .forEach(System.out::println);
	}
	private static void demo3(Stream<President> presidents) {
		presidents.filter(p -> p.getElectionYear() < 2003)
				  .map(President::getFirstname)
				  .forEach(System.out::println);
	}
	private static void demo4(Stream<President> presidents) {
		presidents.filter(p -> p.getElectionYear() < 2003)
				  .map(President::getFirstname)
				  .distinct()
				  .forEach(System.out::println);
	}
	private static void demo5(Stream<President> presidents) {
		presidents.filter(p -> p.getElectionYear() < 2003)
				  .map(President::getFirstname)
				  .distinct()
				  .limit(3)
				  .forEach(System.out::println);
	}
	private static void demo6DONOTDOTHAT(Stream<President> presidents) {
		presidents.filter(p -> p.getElectionYear() < 2003)
				  .map(President::getFirstname)
				  .distinct()
				  .limit(3)
				  .collect(Collectors.toList())
				  .forEach(System.out::println);
	}
	private static void demo7(Stream<President> presidents) {
		presidents.filter(p -> "Tonton".equals(p.getFirstname()))
				  .findAny()
				  .map(President::getFirstname)
				  .ifPresentOrElse(
						  System.out::println
						  , () -> { 
							  System.out.println("WAT");
						  }); 
				
	}

	private static Predicate<? super President> predicateIsTonton() {
		return p -> "Tonton".equals(p.getFirstname());
	}
	
	private static Optional<President> findTonton(Stream<President> presidents){
		return presidents.filter(predicateIsTonton()).findAny();
	}
	
	private static void demo7JDK9(Stream<President> presidents) {
		//optional.stream added to jdk9
		Stream<President> streamOfOneOrZeroTonton = StreamsDemo.findTonton(presidents).stream();
	}
	
	private static void demo8(Stream<President> presidents) {
		List<President> listOfOneOrZero = StreamsDemo.findTonton(presidents)
				.map(Collections::singletonList)
				.orElse(Collections.emptyList());
	}
	private static void demo9NOPE(Stream<President> presidents) {
		President tonton = StreamsDemo.findTonton(presidents).get();
		Stream<Stream<Enfant>> wtf = tonton.getFemmes().stream().map(w  -> w.getEnfants().stream());
	}
	private static void demo10MEH(Stream<President> presidents) {
		President tonton = StreamsDemo.findTonton(presidents).get();
		tonton.getFemmes()
			  .stream()
			  .map(w -> w.getEnfants())
			  .flatMap(List::stream)
			  .map(c -> c.getNom())
			  .forEach(System.out::println);
	}
	private static void demo11(Stream<President> presidents) {
		//in java 9 just .stream
		StreamsDemo.findTonton(presidents).map(Stream::of).orElseGet(Stream::empty)
				   .map(t -> t.getFemmes())
				   .flatMap(List::stream)
				   .map(w -> w.getEnfants())
				   .flatMap(List::stream)
				   .map(c -> c.getNom())
				   .forEach(System.out::println);
	}
	
	private static void demo12(Stream<President> presidents) {
		//in java 9 just .stream
		StreamsDemo.findTonton(presidents).map(Stream::of).orElseGet(Stream::empty)
				   .map(t -> t.getFemmes())
				   .flatMap(List::stream)
				   .map(w -> w.getEnfants())
				   .flatMap(List::stream)
				   .sorted(Comparator.comparing(Enfant::getAge)/*.reversed()*/)
				   .map(c -> c.getNom())
				   .forEach(System.out::println);
	}
	private static void demo13(Stream<President> presidents) {
		Map<Integer, Enfant> children = StreamsDemo.findTonton(presidents).stream()
				   .map(t -> t.getFemmes())
				   .flatMap(List::stream)
				   .map(w -> w.getEnfants())
				   .flatMap(List::stream)
				   .sorted(Comparator.comparing(Enfant::getAge)/*.reversed()*/)
				   //.collect(Collectors.toList());
				   .collect(Collectors.toMap(Enfant::getAge, Function.identity()));
	}
}

package streamdemo;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamProof {
	public static class Whatever {
		private final String text;
		private final int    number;
		
		public Whatever(final String text, final int number) {
			this.number = number;
			this.text   = text;
		}
		
		public String getText() {
			return this.text;
		}
		public int getNumber() {
			return this.number;
		}
	}
	
	public static void main(String... args) {
		List<Whatever> whtvr = Arrays.asList(
				new Whatever("and" , 120),
				new Whatever("wat" , 189),
				new Whatever("everything" , 132),
				new Whatever("42" , 24),
				new Whatever("1138" , 177),
				new Whatever("to" , 72),
				new Whatever("answer" , 60),
				new Whatever("for" , 333),
				new Whatever("universe" , 108),
				new Whatever(":" , 36),
				new Whatever("all" , 698),
				new Whatever("in" , 170),
				new Whatever("." , 144),
				new Whatever("life" , 84),
				new Whatever("The" , 48),
				new Whatever("By" , 199),
				new Whatever("the" , 96)
		);
		
		//garder les divisibles par 12, trier par ordre croissant, transformer en string et afficher
		System.out.println(StreamProof.doWithoutStreams(whtvr));
		System.out.println(StreamProof.doWithStreams(whtvr.stream()));
	}
	
	public static class WhateverComparator implements Comparator<Whatever> {
		@Override
		public int compare(Whatever w1, Whatever w2) {
			return w1.getNumber() - w2.getNumber();
		}
	}
	
	public static String doWithoutStreams(List<Whatever> whtvrs) {
		String __ = "";
		Collections.sort(whtvrs, new StreamProof.WhateverComparator());
		for (int i = 0; i < whtvrs.size(); ++i) {
			Whatever whtvr = whtvrs.get(i); 
			if (whtvr.getNumber() % 12 == 0) {
				__ += whtvr.getText()+" ";
			}
		}
		return __;
	}
	
	public static String doWithStreams(Stream<Whatever> whtvrStream) {
		return whtvrStream.filter(w -> w.getNumber() % 12 == 0 )
						  .sorted(Comparator.comparing(Whatever::getNumber))
						  .map(Whatever::getText)
						  .collect(Collectors.joining(" "));
	}
	
}

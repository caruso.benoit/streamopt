package carinsurance;

import java.util.ArrayList;
import java.util.List;

import carinsurance.CarInsurance.*;

public class DataFactory {
	public static List<Person> getPersons(){
		List<Person> persons = new ArrayList<>();
		
		persons.add(new Person(new Car(new Insurance("GA"))));
		persons.add(new Person(new Car(new Insurance("BU"))));
		persons.add(new Person(new Car(new Insurance("ZO"))));
		persons.add(new Person(new Car(new Insurance("MEUH"))));
		persons.add(new Person(new Car(null)));
		persons.add(new Person(null));
		persons.add(null);
		
		return persons;
	}
}

package carinsurance;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

public class CarInsuranceFinal {
	public static class Car {
		private Optional<Insurance> insurance;
		public Car() { this.insurance = Optional.empty(); }
		public Car(Insurance insurance) { this.insurance = Optional.of(insurance); }
		public Optional<Insurance> getInsurance(){ return insurance; }
	}
	
	public static class Person {
		private Optional<Car> car;
		public Person() { this.car = Optional.empty(); }
		public Person(Car car) { this.car = Optional.of(car); }
		public Optional<Car> getCar() { return this.car; }
	}
	public static class Insurance {
		private String name;
		public Insurance(String name) { this.name = name; }
		public String getName() { return name; }
	}
	
	//récupérer le nom de l'assurance de la personne
	public static String getCarInsuranceName(Optional<Person> person) {
		return person.flatMap(Person::getCar)
		             .flatMap(Car::getInsurance)
		             .map(Insurance::getName)
		             .orElse("Unknown");
	}
	
	
	public static void main(String[] args) {
		Function<Optional<Person>, String> getCarInsuranceName = CarInsuranceFinal::getCarInsuranceName;
		Consumer<String>                   print               = System.out::println;  
		Consumer<Optional<Person>>         composed            = person -> print.accept(getCarInsuranceName.apply(person));
		
		DataFactoryOptional.getPersons().forEach(composed);
	}

}

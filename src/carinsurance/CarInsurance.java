package carinsurance;

import java.util.function.Consumer;
import java.util.function.Function;

public class CarInsurance {
	public static class Car {
		private Insurance insurance;
		public Car(Insurance insurance) { this.insurance = insurance; }
		public Insurance getInsurance(){ return insurance; }
	}
	
	public static class Person {
		private Car car;
		public Person(Car car) { this.car = car; }
		public Car getCar() { return this.car; }
	}
	public static class Insurance {
		private String name;
		public Insurance(String name) { this.name = name; }
		public String getName() { return name; }
	}
	
	//récupérer le nom de l'assurance de la personne
	public static String getCarInsuranceName(Person person) {
		if(person != null) {
			Car car = person.getCar();
			if (car != null) {
				Insurance insurance = car.getInsurance();
				if (insurance != null) {
					return insurance.getName();
				}
			}
		}
		return "Unknown";
	}
	
	
	public static void main(String[] args) {
		Function<Person, String> getCarInsuranceName = CarInsurance::getCarInsuranceName;
		Consumer<String>         print               = System.out::println;  
		Consumer<Person>         composed            = person -> print.accept(getCarInsuranceName.apply(person));
		
		DataFactory.getPersons().forEach(composed);
	}

}

package carinsurance;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import carinsurance.CarInsuranceFinal.*;

public class DataFactoryOptional {
	public static List<Optional<Person>> getPersons(){
		List<Optional<Person>> persons = new ArrayList<>();
		
		persons.add(Optional.of(new Person(new Car(new Insurance("GA")))));
		persons.add(Optional.of(new Person(new Car(new Insurance("BU")))));
		persons.add(Optional.of(new Person(new Car(new Insurance("ZO")))));
		persons.add(Optional.of(new Person(new Car(new Insurance("MEUH")))));
		persons.add(Optional.of(new Person(new Car())));
		persons.add(Optional.of(new Person()));
		persons.add(Optional.empty());
		
		return persons;
	}
}

package data;

import java.util.List;

public class Femme {
	private final String nom;
	private final List<Enfant> enfants;
	
	public Femme(final String nom, final List<Enfant> enfants) {
		this.nom = nom;
		this.enfants = enfants;
	}

	public String getNom() {
		return nom;
	}

	public List<Enfant> getEnfants() {
		return enfants;
	}
}

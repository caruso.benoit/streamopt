package data;

public class Enfant {
	private final String nom;
	private final int age;
	public Enfant(final String nom, final int age) {
		this.nom = nom;
		this.age = age;
	}
	public String getNom() {
		return nom;
	}
	public int getAge() {
		return age;
	}
	
	
}

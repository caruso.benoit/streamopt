package data;

import java.util.Collections;
import java.util.List;

public class President {
	private final String firstname;
	private final String lastname;
	private final int electionYear;
	private final String party;
	
	private List<Femme> femmes = Collections.emptyList();
	
	public President(final String firstName, final String lastName, final int electionYear, final String party, final List<Femme> femmes) {
		this(firstName, lastName, electionYear, party);
		this.femmes = femmes;
	}
	
	public President(final String firstName, final String lastName, final int electionYear, final String party) {
		this.electionYear = electionYear;
		this.lastname = lastName;
		this.firstname = firstName;
		this.party = party;
	}
	
	public String getFirstname() {
		return firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public int getElectionYear() {
		return electionYear;
	}
	public String getParty() {
		return party;
	}

	public List<Femme> getFemmes() {
		return femmes;
	}
	
}

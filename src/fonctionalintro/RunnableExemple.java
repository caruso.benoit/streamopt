package fonctionalintro;

public class RunnableExemple {

	public static void main(String[] args) {
		
		Runnable r = new Runnable() {
		 	@Override
		    public void run() {
		        System.out.println("Hello!");
		    }
		};
		
		((Runnable)() -> System.out.println("Hello!")).run();
	}

}

package fonctionalintro;

import java.io.Serializable;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class LambdasFinal {
	public static void main(String... args) {
		String henry = "Henry";
		//must be final or effectively final
		//henry = "zzz";
		//sayHi(() -> { henry= "lol"; return henry;}, (String name) -> "M. " + name);
		
		//sayHi((NameSupplier & Serializable) () -> henry, (String name) -> "M. " + name);
		
		Predicate<String> isHenry = name -> "Henry".equals(name);
		
		sayHi((NameSupplier & Serializable) () -> henry, (String name) -> "M. " + name, isHenry, name -> "GET OUT " + name);
		
		doSomething(name -> System.out.print(name));
	}
	
	private static void sayHi(NameSupplier nameSupplier, Function<String, String> decorator, Predicate<String> isHenry, Function<String, String> onFail) {
		String name = nameSupplier.get();
		if (isHenry.test(name)) {
			System.out.println("Hello " + decorator.apply(nameSupplier.get()));
		}else {
			System.out.println(onFail.apply(name));
		}
	}
	
	private static void doSomething(Consumer<String> consumer) {
		consumer.accept("Henry");
	}
	
	@FunctionalInterface
	private interface NameSupplier {
		String get();
	}
}

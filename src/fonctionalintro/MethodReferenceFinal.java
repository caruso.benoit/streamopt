package fonctionalintro;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;

public class MethodReferenceFinal {

	public static void main(String... args) {
		List<Train> trains = Collections.unmodifiableList(Arrays.asList(new BlueTrain(), new GreenTrain()));
		//foreach a comme param une fonction d'ordre supérieur -> Consumer<Train>
		//trains.forEach(t -> t.repair());
		
//		trains.forEach(t -> {
//			if (t.isBlue()) {
//				t.repair();	
//			}
//		}); //attention lambda deviens sale si trop de choses dedans -> remplacer par ref de méthode
		
		//méthode de classe
		trains.forEach(Train::repair); // Overload en fonction du type de train :D
		
		//methode  statique avec train en param
		trains.forEach(Train::paintBlue); 
		
		//instance d'une autre classe avec Train en param
		Passenger p = new Passenger();
		trains.forEach(p::inboard);
		
		//Plus bizarre -> reference de constructeur -> new (constructeur par défaut) est donc un Supplier !
		//mais attention le new dépends du contexte et donc n'est pas forcément un supplier, peut être une fonction par ex
		List<Train> trainsConstMethodRef = Collections.unmodifiableList(Arrays.asList(Train.create(BlueTrain::new), new GreenTrain()));
		
	}
	
	public static class Passenger{
		public void inboard(Train train) {
			System.out.println("Inboard " + train);
		}
	}
	
	public interface Train {
		static Train create(Supplier<Train> trainSupplier) {
			return trainSupplier.get();
		}
		static void paintBlue(Train train) {
			System.out.println("Painting blue "+train);
		}
		boolean isBlue();
		void repair();
	}
	public static class BlueTrain implements Train {		
		@Override
		public boolean isBlue() {
			return true;
		}

		@Override
		public void repair() {
			System.out.println("Repaired blue train "+ this);
		}
	}
	public static class GreenTrain implements Train {
		@Override
		public boolean isBlue() {
			return false;
		}

		@Override
		public void repair() {
			System.out.println("Repaired green train "+this);
		}
		
	}
}

package fonctionalintro;

import java.util.function.Consumer;
import java.util.function.Supplier;

public class Lambdas {
	public static void main(String... args) {
		sayHi("Henry");
	}
	
	private static void sayHi(String name) {
		System.out.println("Hello " + name);
	}
	
	private static void doSomething(Consumer<String> consumer) {
		consumer.accept("Henry");
	}
	
	@FunctionalInterface
	private interface NameSupplier {
		String get();
	}
}

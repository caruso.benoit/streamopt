package fonctionalintro;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;

public class MethodReference {

	public static void main(String... args) {
		List<Train> trains = Collections.unmodifiableList(Arrays.asList(new BlueTrain(), new GreenTrain()));
		trains.forEach(t -> t.repair());
	}
	
	public static class Passenger{
		public void inboard(Train train) {
			System.out.println("Inboard " + train);
		}
	}
	
	public interface Train {
		static Train create(Supplier<Train> trainSupplier) {
			return trainSupplier.get();
		}
		static void paintBlue(Train train) {
			System.out.println("Painting blue "+train);
		}
		boolean isBlue();
		void repair();
	}
	public static class BlueTrain implements Train {		
		@Override
		public boolean isBlue() {
			return true;
		}

		@Override
		public void repair() {
			System.out.println("Repaired blue train "+ this);
		}
	}
	public static class GreenTrain implements Train {
		@Override
		public boolean isBlue() {
			return false;
		}

		@Override
		public void repair() {
			System.out.println("Repaired green train "+this);
		}
		
	}
}
